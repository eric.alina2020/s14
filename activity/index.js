// A function that adds two numbers...
function addNum(x, y){
	addition = x + y;
	return "The sum of " + x + " and " + y + " is " + addition + ".";
};

let numSum = addNum(25, 25);
console.log(numSum);


// A function that substracts two numbers...
function subtractNum(a, b){
	subtraction = a - b;
	return "The difference of " + a + " and " + b + " is " + subtraction + ".";
};

let numDif = subtractNum(4, 8);
console.log(numDif);


// A function that multiplies two numbers...
function multiplyNum(p, q){
	multiplication = p * q;
	return "The product of " + p + " and " + q + " is " + multiplication + ".";
};

let product = multiplyNum(4, 10);
console.log(product);

